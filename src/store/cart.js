import * as fb from 'firebase'

class Cart {
  constructor (name, price, img, date, count, id = null) {
    this.img = img
    this.name = name
    this.price = price
    this.date = date
    this.count = count
    this.id = id
  }
}

export default{
  state: {
    cartList: []
  },
  mutations: {
    addProd (state, payload) {
      state.cartList.push(payload) // Push elements in local Array cartList
    },
    loadProd (state, payload) {
      state.cartList = payload
    },
    deleteProduct (state, id) {
      const item = state.cartList.map(item => item.id).indexOf(id) // find index of your object
      state.cartList.splice(item, 1)
    },
    deleteAll (state) {
      state.cartList = []
    }
  },
  actions: {
    async addProd ({commit}, payload) { // Add cart elements in database
      try {
        const newProd = new Cart(
          payload.name,
          payload.price,
          payload.img,
          payload.date,
          payload.count,
          payload.id
        )

        const fbVal = await fb.database().ref('cartList').push(newProd)
        commit('addProd', {
          ...newProd,
          id: fbVal.key
        })
      } catch (error) {
        console.log(error)
      }
    },
    async fetchCart ({commit}) { // Get cart elements from database
      const resultProd = [] // Array from default database elements

      try {
        const fbVal = await fb.database().ref('cartList').once('value') // Get default database elements
        const elems = fbVal.val()
        console.log(elems)

        Object.keys(elems).forEach(key => { // Push default elements to local Array resultProd
          const res = elems[key]
          resultProd.push(
            new Cart(res.name, res.price, res.img, res.date, res.count, key)
          )
        })

        commit('loadProd', resultProd)
      } catch (error) {
        console.log(error)
      }
    },
    async updateCount ({commit}, payload) {
      try {
        await fb.database().ref('cartList').child(payload.id).update({count: payload.count})
      } catch (error) {
        console.log(error)
      }
    },
    async deleteProduct ({commit}, payload) {
      try {
        await fb.database().ref('cartList').child(payload).remove()
        commit('deleteProduct', payload)
      } catch (error) {
        console.log(error)
      }
    },
    async deleteAll ({commit}) {
      try {
        await fb.database().ref('cartList').remove()
        commit('deleteAll')
      } catch (error) {
        console.log(error)
      }
    }
  },
  getters: {
    getCartProduct (state) {
      return state.cartList.sort((a, b) => a.date < b.date) // Get cart elements sorting by date
    }
  }
}
