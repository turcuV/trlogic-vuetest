export default {
  state: {
    productList: [
      {
        img: 'https://i-cdn.phonearena.com/images/phones/69193-specs/Apple-iPhone-X.jpg',
        name: 'Iphone',
        price: 800
      },
      {
        img:
            'https://static.fnac-static.com/multimedia/Images/FR/MDM/51/72/6d/7172689/1540-1/tsp20180628151028/Smartphone-Samsung-Galaxy-A8-Double-SIM-32-Go-Noir-Carbone.jpg',
        name: 'HTC',
        price: 500
      },
      {
        img: 'https://www.catphones.com/pub/media/catalog/product/cache/small_image/340x400/beff4985b56e3afdbeabfc89641a4582/c/a/cat-s30-885x968px-1.jpg',
        name: 'Cat',
        price: 380
      },
      {
        img: 'https://boutique.orange.fr/media-cms/mediatheque/636x900-doro-8042-noir---vue-1-107931.jpg',
        name: 'Doro',
        price: 270
      },
      {
        img:
            'https://consumer-img.huawei.com/content/dam/huawei-cbg-site/common/mkt/list-image/phones/mate-9-pro/mate-9-pro-listimage-black.png',
        name: 'Huawei',
        price: 500
      },
      {
        img: 'https://boutique.orange.fr/media-cms/mediatheque/636x900-huawei-p8-mystic-champagne-vue-1-31487.jpg',
        name: 'Huawei',
        price: 600
      }
    ]
  },
  getters: {
    getProduct (state) {
      return state.productList
    }
  }
}
