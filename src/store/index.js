import Vue from 'vue'
import Vuex from 'vuex'
import List from './product-list'
import Cart from './cart'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    List, Cart
  }
})
