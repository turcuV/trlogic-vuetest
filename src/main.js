// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import * as fb from 'firebase'

Vue.config.productionTip = false

/* eslint-disable */

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created () {
    fb.initializeApp({
      apiKey: "AIzaSyALaTBCIhp0jeH3mai2j8ZRRJ2pP8Id7EY",
      authDomain: "vue-cart-57e50.firebaseapp.com",
      databaseURL: "https://vue-cart-57e50.firebaseio.com",
      projectId: "vue-cart-57e50",
      storageBucket: "vue-cart-57e50.appspot.com",
      messagingSenderId: "835562799185"
    })
    this.$store.dispatch('fetchCart')
  }
})
